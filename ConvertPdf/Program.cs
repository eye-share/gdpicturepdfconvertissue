﻿using GdPicture14;
using System;
using System.IO;

namespace ConvertPdf
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "pdffile.pdf");
            var outFile = Path.Combine(Directory.GetCurrentDirectory(), "outfile.tif");
            
            Console.WriteLine($"Reading file {filePath}");

            using var pdfStream = File.OpenRead(filePath);
            using var converter = new GdPictureDocumentConverter();

            var status = converter.LoadFromStream(pdfStream, GdPicture14.DocumentFormat.DocumentFormatPDF);

            if (status == GdPictureStatus.OK)
            {
                converter.RasterizationDPI = 300f;
                converter.SaveAsTIFF(outFile, TiffCompression.TiffCompressionCCITT4);
                Console.WriteLine($"Converted file {outFile}");
            }
            else
            {
                throw new Exception($"Failed to read file:. LoadStatus={status}");
            }
        }
    }
}
